const actions = require("../utils/actions");

// Pages:
const homePage = require("../pages/home.page");
const loginPage = require("../pages/auth/login.page");
const resgiterPage = require("../pages/auth/resgister.page");
const accountPage = require("../pages/account.page");

describe("Smoke Tests", function() {
    beforeEach(function() {
        homePage.visit();
        actions.closeCookiePopup();
        cy.wait(500);
    });
    it.only("successfully logs in", function() {
        loginPage.visit();
        loginPage.login("test.automation@test.com", "CorrectPassword1234");
        accountPage.buttonLogOut.should("be.visible");
    });
    it("1 - LIVE, Customer registers with UK address.", function() {
        loginPage.visit();
        const jsDateNow = Date.now();
        const userEmail = `qa_test+${jsDateNow}@g4m.co.uk`;
        resgiterPage.register(userEmail);
        const currentUrl = cy.url();
        currentUrl.should("include", "/registration/register-success");
    });
    it("2 - LIVE, Customer registers with NON-UK address.", function() {
        loginPage.visit();
        const jsDateNow = Date.now();
        const userEmail = `qa_test+${jsDateNow}@g4m.co.uk`;
        resgiterPage.register(userEmail, "Password1", "France");
        const currentUrl = cy.url();
        currentUrl.should("include", "/registration/register-success");
    });
});