# Example Frontend Automation using Cypress - Mocha Framework:
#### Author: 
Paul Joseph Farrell
#### Email: 
pauljosephfarrell89@gmail.com
#### LinkedIn: 
[Feel free to add me on Linked In](https://www.linkedin.com/in/faz540/)
#### The Objective Of This Repository:
The aim of this repository is to act as a tutorial for fellow testers that want to start writing frontend automation.
The second aim is to act as a 'portfolio' to demonstrate how I write frontend automation for any future employers.

This is my first time using Cypress so any feedback or criticim is greatly appreciated.
## Prerequisite/ Installation:
To clone this repository, you'll obviously need [Git](https://git-scm.com/download/win)
The above assumes you're using Windows...

You must have [Node.js](https://www.nodejs.org/) installed.

This test will run against Chrome, so make sure you have the Chrome browser installed.

## Install Dependencies:
```
npm install
```
## Opening the Cypress application:
```
npm run cypress:open
```
This will open the frontend Cypress application, this may take a while if it's the first time you're opening Cypress.
Once opened, you can run the tests in the Cypress application which shows you a live test run of all the test running.
It's reminiscint of record and playback tools like Selenium IDE and Ghost Inspector.
This is really helpful with debugging and it's currently set up to re-run the tests whenever there are changes saved to any of the test files.

## Running the tests in the terminal:
```
npm test
```
This will run all test files in the 'tests' directory (there's only one) in a headless browser.
This will output a spec report (Cypress supports several reporting tools)