class Actions {
    clearCookies() {
        return cy.clearCookies();
    };

    closeCookiePopup() {
        return cy.get('.g4m-btn-tertiary').click();
    };
};

module.exports = new Actions();