class AccountPage {

    get buttonLogOut() { return cy.get('.my-acc-btn').last();}

    visit() {
        return cy.visit('/my-account')
    };
};

module.exports = new AccountPage();