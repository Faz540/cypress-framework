const loginPage = require("./login.page");

// This is the registration form of the /auth/login/ page
class ResgiterPage {
   get textField_email() { return cy.get("#email_address"); }
   get textField_firstName() { return cy.get("#forename"); }
   get textField_lastName() { return cy.get("#lastname"); }
   get textField_telephone() { return cy.get("#telephone"); }
   get dropdown_country() { return cy.get("#country"); }
   get textField_postCode() { return cy.get("#postal_code"); }
   get textField_addressLine1() { return cy.get("#address_1"); }
   get link_manualAddress() { return cy.get(".expand-select .link").first(); }
   get textField_addressLine2() { return cy.get("#address_2"); }
   get textField_city() { return cy.get("#address_3"); }
   get textField_password() { return cy.get("#password_1"); }
   get textField_confirmPassword() { return cy.get("#password_2"); }
   get button_register() { return cy.get("#new-customer-registration-form [type='submit']").first(); }

   visit() {
       return loginPage.visit();
   };

   // Registration:
   register(email, password = "Password1", country = "United Kingdom") {
       this.link_manualAddress.click();
       this.textField_email.type(email);
       this.textField_firstName.type("Automated");
       this.textField_lastName.type("Test");
       this.textField_telephone.type("123456");
       this.dropdown_country.select(country);
       this.textField_postCode.type("WR5 3DA");
       this.textField_addressLine1.type("Loqate");
       // Wait for PCA Predict
       cy.wait(500);
       // Close PCA Predict
       this.textField_addressLine1.type("{esc}");
       this.textField_addressLine2.type("Waterside, Basin Road");
       this.textField_city.type("Worcester");
       this.textField_password.type(password);
       this.textField_confirmPassword.type(password);
       return this.button_register.click();
   };
};

module.exports = new ResgiterPage();