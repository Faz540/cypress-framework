class LoginPage {
    // Declare elements on Login Page:
    get textField_email() { return cy.get("[name='str_email']").last(); }
    get textField_password() { return cy.get("[name='str_password']").last(); }
    get link_forgottenPassword() { return cy.get("[data-target='#password-reset-modal']").last()};
    get button_login() { return cy.get(".btn-primary[value='Log in']").last(); }

    visit() {
        return cy.visit('/auth/login')
    };

    enterCredentials(email = "test.automation@test.com", password="CorrectPassword1234") {
        // The page is not responsive, so the elements are duplicated (One for web, another for Mobile)
        // Therefore the email field elements for both web and mobile share the same IDs.
        // Hence the below...
        this.textField_email.type(email);
        return this.textField_password.type(password);
    };

    login(email = "test.automation@test.com", password="CorrectPassword1234") {
        this.enterCredentials(email, password);
        // The below is written that way as there's a 'Log In' in the footer
        // No unique selector for the Log In button in the form.
        return this.button_login.click();
    };
};

module.exports = new LoginPage();